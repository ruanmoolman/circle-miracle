module moolman.ruan/circleMiracle

go 1.16

require (
	github.com/icza/mjpeg v0.0.0-20210726201846-5ff75d3c479f
	github.com/tdewolff/canvas v0.0.0-20211230013015-6230030f0d2d
	github.com/teris-io/cli v1.0.1
	gonum.org/v1/plot v0.10.0
)
