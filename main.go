package main

import (
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"math"
	"os"
	"strconv"

	"github.com/tdewolff/canvas"
	"github.com/tdewolff/canvas/renderers/rasterizer"
	"github.com/teris-io/cli"
)

func radialPos(r, a float64) (x, y float64) {
	return r * math.Cos(a), r * math.Sin(a)
}

func createDotDrawer(size float64) func(ctx *canvas.Context, pos canvas.Point) {
	dot := canvas.Circle(size)
	return func(ctx *canvas.Context, pos canvas.Point) {
		ctx.DrawPath(pos.X, pos.Y, dot)
	}
}

func drawLine(c *canvas.Context, posA, posB canvas.Point) {
	p := &canvas.Path{}
	p.MoveTo(posA.X, posA.Y)
	p.LineTo(posB.X, posB.Y)
	c.DrawPath(0, 0, p)
}

type Gif struct {
	images  []*image.Paletted
	palette color.Palette
}

func (g *Gif) SaveAs(path string, fps float64) error {
	fmt.Printf("Saving gif to %v\n", path)
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()

	delays := make([]int, len(g.images))
	disposals := make([]byte, len(g.images))
	const maxFramesPerSecond = 100
	if fps > maxFramesPerSecond {
		fmt.Printf("Warning: gif supports max of 100 fps, requested fps %v\n", fps)
		fps = maxFramesPerSecond
	}
	// NOTE: Frame delay of 1 is broken
	frameDelay := int(maxFramesPerSecond / fps)
	for i := range g.images {
		delays[i] = frameDelay
		disposals[i] = gif.DisposalBackground
	}
	err = gif.EncodeAll(f, &gif.GIF{Image: g.images, Delay: delays, Disposal: disposals})
	if err != nil {
		return err
	}
	return nil
}

func (g *Gif) Append(i image.Image) {
	pi := image.NewPaletted(i.Bounds(), g.palette)
	for x := 0; x < i.Bounds().Max.X; x++ {
		for y := 0; y < i.Bounds().Max.Y; y++ {
			pi.Set(x, y, i.At(x, y))
		}
	}
	g.images = append(g.images, pi)
}

func tracePoints(numPoints, numShapePoints int, radius, pointRadiusRatio float64, stepsPerRotation int) *canvas.Path {
	path := &canvas.Path{}

	circRatio := float64(numShapePoints) / float64(numPoints)
	innerRadius := radius * circRatio
	moveRadius := radius - innerRadius

	calcPoint := func(angle float64) (x, y float64) {
		return calcPointPos(moveRadius, angle, innerRadius, 0, pointRadiusRatio)
	}

	numRotations := numShapePoints
	angleStepSize := twoPi / float64(stepsPerRotation)

	maxAngle := twoPi * float64(numRotations)

	x, y := calcPoint(0.0)
	path.MoveTo(x, y)
	for a := angleStepSize; a <= maxAngle; a = a + angleStepSize {
		x, y = calcPoint(a)
		path.LineTo(x, y)
	}
	return path
}

func calcPointPos(r0, a0, r1, a1, d float64) (x, y float64) {
	r := r0 + r1
	a := -a0*r/r1 + a0 + a1
	px, py := radialPos(r1, a)
	cx, cy := radialPos(r0, a0)
	return cx + px*d, cy + py*d
}

func calculatePoints(numPoints, numShapePoints int, radius, pointRadiusRatio float64) func(angle float64) []canvas.Point {
	circRatio := float64(numShapePoints) / float64(numPoints)
	innerRadius := radius * circRatio
	moveRadius := radius - innerRadius
	numShape2Points := numPoints - numShapePoints

	return func(angle float64) []canvas.Point {
		points := make([]canvas.Point, 0, numShapePoints*numShape2Points)
		for j := 0; j < numShape2Points; j++ {
			circOffset := twoPi * float64(j) / float64(numShape2Points)
			circAngle := circOffset + angle
			for i := 0; i < numShapePoints; i++ {
				pointOffset := twoPi * float64(i+j) / float64(numShapePoints)
				px, py := calcPointPos(moveRadius, circAngle, innerRadius, pointOffset, pointRadiusRatio)
				points = append(points, canvas.Point{px, py})
			}
		}
		return points
	}
}

func angleSteps(numRotations, stepsPerRotation int) []float64 {
	totalSteps := numRotations * stepsPerRotation
	angleStepSize := twoPi / float64(stepsPerRotation)
	angles := make([]float64, totalSteps)
	for i := 0; i < totalSteps; i++ {
		angles[i] = float64(i) * angleStepSize
	}
	return angles
}

const twoPi = 2 * math.Pi

func mustParseInt(text string, def int) int {
	if text == "" {
		return def
	}
	value, err := strconv.ParseInt(text, 10, 64)
	if err != nil {
		fmt.Printf("Could not convert %v to an integer, using default\n", text)
		return def
	}
	return int(value)
}
func mustParseFloat(text string, def float64) float64 {
	if text == "" {
		return def
	}
	value, err := strconv.ParseFloat(text, 64)
	if err != nil {
		fmt.Printf("Could not convert %v to an integer, using default\n", text)
		return def
	}
	return value
}

const defaultDistance = 0.8
const defaultDuration = 10
const defaultFps = 30
const defaultResolution = 200

func circleMiracle(args []string, options map[string]string) int {
	palette := color.Palette{
		canvas.Transparent,
		canvas.Red,
		canvas.Green,
		canvas.Blue,
	}
	gray := make([]color.Color, 16)
	for i := uint8(0); i < 16; i++ {
		v := math.MaxUint8 / 16 * i
		gray[i] = color.RGBA{v, v, v, math.MaxUint8}
		palette = append(palette, gray[i])
	}

	numPoints := mustParseInt(args[0], 7)
	numShapePoints := mustParseInt(args[1], 3)
	outputDir := "./"
	if len(args) > 2 {
		outputDir = args[2]
	}
	if outputDir[len(outputDir)-1] != '/' {
		outputDir = outputDir + "/"
	}
	verbose := options["verbose"] == "true"

	distance := mustParseFloat(options["offset"], defaultDistance)
	rotationSeconds := mustParseFloat(options["duration"], defaultDuration)
	fps := float64(mustParseInt(options["fps"], defaultFps))
	resolution := float64(mustParseInt(options["resolution"], defaultResolution))

	mainCircleRadius := resolution / 2.0
	lineWidth := resolution / 100

	animation := Gif{palette: palette}
	stepsPerRotation := int(fps * rotationSeconds)

	tps := tracePoints(numPoints, numShapePoints, mainCircleRadius, distance, stepsPerRotation)

	calcPoints := calculatePoints(numPoints, numShapePoints, mainCircleRadius, distance)

	numRotations := numShapePoints
	angles := angleSteps(numRotations, stepsPerRotation)
	res := canvas.Resolution(1)

	cvs := canvas.New(resolution, resolution)
	ctx := canvas.NewContext(cvs)
	bgCol := gray[2]
	ctx.SetFillColor(bgCol)
	// For transparent background one needs to dispose each frame
	ctx.SetStrokeColor(canvas.Darkgray)
	ctx.SetStrokeWidth(lineWidth)
	ctx.SetView(canvas.Identity.Translate(resolution/2, resolution/2))

	cs := canvas.LinearColorSpace{}

	drawDot := createDotDrawer(lineWidth)
	for ai, alpha := range angles {
		if verbose {
			fmt.Println("frame", ai, "of", len(angles))
		} else if ai%(len(angles)/10) == 0 {
			fmt.Printf(".")
		}
		cvs.Reset()
		ctx.SetFillColor(bgCol)
		ctx.SetStrokeColor(bgCol)
		ctx.DrawPath(-resolution/2, -resolution/2, canvas.Rectangle(resolution, resolution))

		ctx.SetStrokeColor(canvas.Darkgray)
		ctx.SetStrokeWidth(lineWidth / 2)
		ctx.DrawPath(0, 0, tps)
		ctx.SetStrokeWidth(lineWidth)
		points := calcPoints(alpha)
		var numShapePoints2 = numPoints - numShapePoints

		ctx.SetStrokeColor(canvas.Green)
		// connect matching circle points
		for i := 0; i < numShapePoints; i++ {
			for j := 0; j < numShapePoints2; j++ {
				piA := i + j*numShapePoints
				piB := i + (j+1)%numShapePoints2*numShapePoints
				drawLine(ctx, points[piA], points[piB])
			}
		}

		// draw "main" shapes after drawing the "secondary" shapes
		ctx.SetStrokeColor(canvas.Blue)
		// connect points in circles
		for i := 0; i < numShapePoints; i++ {
			for j := 0; j < numShapePoints2; j++ {
				piA := numShapePoints*j + i
				piB := numShapePoints*j + (i+1)%numShapePoints
				drawLine(ctx, points[piA], points[piB])
			}
		}

		ctx.SetFillColor(canvas.Red)
		ctx.SetStrokeColor(canvas.Red)
		for _, p := range points {
			drawDot(ctx, p)
		}
		animation.Append(rasterizer.Draw(cvs, res, cs))
		//renderers.Write("canvas.png", cvs, res)
	}

	fileName := fmt.Sprintf("%v-%v-%v_%v_%v.gif", numPoints, numShapePoints, distance, resolution, fps)
	err := animation.SaveAs(outputDir+fileName, fps)
	if err != nil {
		fmt.Println("Failed to save gif:", err.Error())
		return 2
	}

	return 0
}

func main() {
	app := cli.New("Little app I wrote to generate the animations that Mathologer discussed in https://www.youtube.com/watch?v=oEN0o9ZGmOM").
		WithArg(cli.NewArg("star points", "number of points in the star").WithType(cli.TypeInt)).
		WithArg(cli.NewArg("shape points", "number of points in the shape").WithType(cli.TypeInt)).
		WithArg(cli.NewArg("output directory", "file path of the output gif, default .").WithType(cli.TypeString).AsOptional()).
		WithOption(cli.NewOption("duration", fmt.Sprintf("seconds shape to complete one rotation, default %v", defaultDuration)).
			WithChar('d').WithType(cli.TypeInt)).
		WithOption(cli.NewOption("fps", fmt.Sprintf("frames per second, default %v", defaultFps)).
			WithChar('f').WithType(cli.TypeNumber)).
		WithOption(cli.NewOption("offset", fmt.Sprintf("offset (0=center, 1=perimeter) of the point from the center of the rotating circle, default %v", defaultDistance)).
			WithChar('o').WithType(cli.TypeNumber)).
		WithOption(cli.NewOption("resolution", fmt.Sprintf("pixel width and height of the animation, default %v", defaultResolution)).
			WithChar('r').WithType(cli.TypeNumber)).
		WithOption(cli.NewOption("verbose", "output additional information").WithChar('v').WithType(cli.TypeBool)).
		WithAction(circleMiracle)
	os.Exit(app.Run(os.Args, os.Stdout))
}
